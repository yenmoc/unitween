﻿namespace UnityModule.UniTween
{
    public enum UpdateType
    {
        Normal,
        Late,
        Fixed,
        Manual,
        Coroutine
    }
}