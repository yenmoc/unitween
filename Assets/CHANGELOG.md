# Changelog
All notable changes to this project will be documented in this file.

## [0.1.1] - 28-11-2019
### Changed
	-Change the parameter order method Interpolate for same type with Curve `TweenMotion.From(Curve curve, float duration)`

	```csharp

	//before
	public static ITween Interpolate(float duration, Type type)
	public static ITween Interpolate(Velocity velocity, Type type)


	//after
	public static ITween Interpolate(Type type, float duration)
	public static ITween Interpolate(Type type, Velocity velocity)

	```

